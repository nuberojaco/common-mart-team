package org.commonmart.thecommonmart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void search(View view){

        Intent intent = new Intent(this, Search.class);
        //EditText editText = (EditText) findViewById(R.id.editText);
        // String message = editText.getText().toString();
        String message="Welcome";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void cart (View view){
        Intent intent = new Intent(this, Cart.class);
        String message="Gome";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}
